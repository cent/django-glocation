# coding: utf-8
from django.conf import settings
from django.apps import AppConfig


class GlocationConfig(AppConfig):
    name = 'glocation'

    def __init__(self, app_name, app_module):
        super(GlocationConfig, self).__init__(app_name, app_module)
        if not hasattr(settings, 'GLOCATION_DEFAULT_LANGUAGE'):
            setattr(settings, 'GLOCATION_DEFAULT_LANGUAGE', 'en')

        if not hasattr(settings, 'GLOCATION_ALLOWED_LANGUAGES'):
            setattr(settings, 'GLOCATION_ALLOWED_LANGUAGES', ['en', 'uk', 'ru'])

        if not hasattr(settings, 'GLOCATION_CACHE_TIMEOUT'):
            setattr(settings, 'GLOCATION_CACHE_TIMEOUT', 3600)

        if not hasattr(settings, 'GLOCATION_APPEND_ORPHAN'):
            setattr(settings, 'GLOCATION_APPEND_ORPHAN', False)

        if not hasattr(settings, 'GOOGLE_API_KEY'):
            raise ValueError('settings.GOOGLE_API_KEY required!')

        if not hasattr(settings, 'GOOGLE_PUBLIC_API_KEY'):
            setattr(settings, 'GOOGLE_PUBLIC_API_KEY', settings.GOOGLE_API_KEY)
