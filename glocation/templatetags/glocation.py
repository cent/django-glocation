# coding: utf-8
from django.shortcuts import resolve_url
from django.template import Library


__author__ = 'vadim'


register = Library()


@register.simple_tag(takes_context=True)
def location_name(context, location):
    lang = context['request'].LANGUAGE_CODE
    return location.get_name(lang=lang)


@register.simple_tag(takes_context=True)
def place_name(context, place):
    lang = context['request'].LANGUAGE_CODE
    return place.get_name(lang=lang)


@register.simple_tag(takes_context=True)
def place_street_name(context, place):
    lang = context['request'].LANGUAGE_CODE
    if 'street_address' not in place.types:
        return place.get_formatted_parents_name(('route', 'street_number'), lang, ', ')
    return place.get_name(lang=lang)

@register.simple_tag(takes_context=True)
def place_formatted_address(context, place):
    lang = context['request'].LANGUAGE_CODE
    return place.get_formatted_address(lang=lang)


@register.simple_tag(takes_context=True)
def widget_localize(context, bound_field):
    bound_field.field.widget.lang = context['request'].LANGUAGE_CODE
    return bound_field
