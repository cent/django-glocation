# coding: utf-8
import googlemaps
from django.core.cache import cache
from django.conf import settings
from django.utils.functional import SimpleLazyObject


__author__ = 'vadim'


gmap_api = SimpleLazyObject(lambda: googlemaps.Client(key=settings.GOOGLE_API_KEY))


GLOCATION_KEY_FORMAT_LATLNG ='glocation_latlng_{latlng}_{result_type}_{lang}'
GLOCATION_KEY_FORMAT_PLACE_ID = 'glocation_place_id_{place_id}_{lang}'


def get_place(place_id, lang=None):
    lang = lang or settings.GLOCATION_DEFAULT_LANGUAGE
    key = GLOCATION_KEY_FORMAT_PLACE_ID.format(place_id=place_id, lang=lang)
    data = cache.get(key)
    if not data:
        data = gmap_api.place(place_id=place_id, language=lang)
        if data['status'] == 'OK':
            cache.set(key, data, settings.GLOCATION_CACHE_TIMEOUT)
        else:
            raise ValueError('Error get place_id: %s\n%s' % (place_id, data))
    return data['result']


def get_reverse_geocode(latlng, result_type, lang=None):
    lang = lang or settings.GLOCATION_DEFAULT_LANGUAGE
    key = GLOCATION_KEY_FORMAT_LATLNG.format(latlng=latlng, result_type=result_type, lang=lang)
    data = cache.get(key)
    if not data:
        data = gmap_api.reverse_geocode(latlng=latlng, result_type=result_type, language=lang)
        if data:
            cache.set(key, data, settings.GLOCATION_CACHE_TIMEOUT)
    return data
