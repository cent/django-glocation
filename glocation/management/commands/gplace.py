# coding: utf-8
from django.contrib.gis.geos import Point
from django.core.management import BaseCommand
from glocation.models import Glocation, Place

__author__ = 'vadim'


class Command(BaseCommand):
    """
    update google place
    """
    def add_arguments(self, parser):
        parser.add_argument('command', type=str)
        parser.add_argument('resource', type=str)
        parser.add_argument('resource_id', type=str, default=None)
        parser.add_argument('--lang', dest='lang', type=str, default='en')
        parser.add_argument('--empty', action='store_true', dest='empty', default=True, help='process empty records')

    def handle(self, *args, **options):
        lang = options['lang']
        if options['command'] == 'update':
            if options['resource'] == 'place':
                node = Glocation.get_location(options['resource_id'])
                print('Update place (%s): %s' % (node.place_id, node.name.get(lang)))
        elif options['command'] == 'transform':
            if options['resource'] == 'places':
                if options['resource_id'] == 'all':
                    for node in Place.objects.all():
                        node.point = Point(x=node.point.y, y=node.point.x)
                        node.save()
                        print('Update place (%s): %s' % (node.id, node.name.get(lang)))
                else:
                    node = Place.objects.get(id=options['resource_id'])
                    node.point = Point(x=node.point.y, y=node.point.x)
                    node.save()
                    print('Update place (%s): %s' % (node.id, node.name.get('en')))

            elif options['resource'] == 'glocations':
                if options['resource_id'] == 'all':
                    for node in Glocation.objects.all():
                        node.point = Point(x=node.point.y, y=node.point.x)
                        node.save()
                        print('Update place (%s): %s' % (node.place_id, node.name.get(lang)))
                else:
                    node = Glocation.objects.get(place_id=options['resource_id'])
                    node.point = Point(x=node.point.y, y=node.point.x)
                    node.save()
                    print('Update place (%s): %s' % (node.place_id, node.name.get(lang)))
        elif options['command'] == 'update_name':
            if options['resource'] == 'glocation':
                if options['resource_id'] == 'all':
                    for node in Glocation.objects.all():
                        if not options['empty'] or not node.name.get(lang):
                            print('Update place (%s) %s: %s -> %s' % (node.place_id,
                                                                      lang,
                                                                      node.name.get(lang),
                                                                      node.get_name(lang)))
                else:
                    node = Glocation.get_location(options['resource_id'])
                    if not options['empty'] or not node.name.get(lang):
                        print('Update place (%s) %s: %s -> %s' % (node.place_id,
                                                                  lang,
                                                                  node.name.get(lang),
                                                                  node.get_name(lang)))

            elif options['resource'] == 'place':
                if options['resource_id'] == 'all':
                    for node in Place.objects.all():
                        if not options['empty'] or not node.name.get(lang):
                            print('Update place (%s) %s: %s -> %s' % (node.id,
                                                                      lang,
                                                                      node.name.get(lang),
                                                                      node.get_name(lang)))
                else:
                    node = Place.objects.get(id=options['resource_id'])
                    if not options['empty'] or not node.name.get(lang):
                        print('Update place (%s) %s: %s -> %s' % (node.id,
                                                                  lang,
                                                                  node.name.get(lang),
                                                                  node.get_name(lang)))




