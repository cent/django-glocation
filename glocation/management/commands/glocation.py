# coding: utf-8
from django.contrib.gis.geos import Point
from django.core.management import BaseCommand
from glocation.models import Glocation, Place

__author__ = 'vadim'


class Command(BaseCommand):
    """
    update google place
    """
    def add_arguments(self, parser):
        parser.add_argument('command', type=str)
        parser.add_argument('resource', type=str)
        parser.add_argument('resource_id', type=str, default=None)
        parser.add_argument('--lang', dest='lang', type=str, default='ru')

    def handle(self, *args, **options):
        if options['command'] == 'get':
            if options['resource'] == 'place':
                node = Glocation.get_location(options['resource_id'])
                if not node:
                    print('PlaceID: %s not found!' % options['resource_id'])
                    return
                if node.place_id == options['resource_id']:
                    place = node.place
                else:
                    place = node.place_related

                print('%s: placeID found: %s' % (options['resource_id'],  place.get_name(options['lang'])))

                while node:
                    print('{}: {}> {} -> {}'.format(node.place_id,
                                                    '===' * node.get_depth(),
                                                    node.type,
                                                    node.get_name(options['lang'])))
                    node = node.get_parent()

