# coding: utf-8
import logging
from datetime import timedelta
from django.contrib.gis.db.models import PointField
from django.contrib.gis.geos import Point
from django.contrib.postgres.fields import JSONField, ArrayField
from django.db.transaction import atomic
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.conf import settings
from django.db import models
from treebeard.mp_tree import MP_Node
from .utils import get_place, get_reverse_geocode


logger = logging.getLogger(__name__)


class Place(models.Model):
    """ Cached model Google PlaceID """

    id = models.CharField(primary_key=True, max_length=255)
    types = ArrayField(models.CharField(_('types'), max_length=32))
    point = PointField(blank=True, null=True)
    name = JSONField(blank=True, default={})  # different language
    formatted_address = JSONField(blank=True, default={})
    parents_name = JSONField(blank=True, default={})  # formatted address_components
    parents = JSONField(blank=True, default={})  # related types

    date_create = models.DateTimeField(_('date create'), auto_now_add=True)
    date_update = models.DateTimeField(_('date update'), auto_now=True)

    ALLOWED_PARENTS_NAME = ['street_number', 'route', 'sublocality', 'locality', 'administrative_area_level_1',
                            'country', 'postal_code']

    class Meta:
        app_label = 'glocation'
        db_table = 'place'
        verbose_name = _('Google place')
        verbose_name_plural = _('Google places')

    def __str__(self):
        return '%s<%s:%s>' % (self.__class__.__name__, self.id, self.get_name())

    @classmethod
    def _lang(cls, lang):
        lang = lang or settings.GLOCATION_DEFAULT_LANGUAGE
        if lang not in settings.GLOCATION_ALLOWED_LANGUAGES:
            lang = settings.GLOCATION_DEFAULT_LANGUAGE
        return lang

    def get_name(self, lang=None):
        lang = self._lang(lang)
        if lang not in self.name:
            data = get_place(place_id=self.id, lang=lang)
            self.update_place(data=data, lang=lang)
            self.save()
        return self.name[lang]

    def get_formatted_address(self, lang=None):
        lang = self._lang(lang)
        if lang not in self.formatted_address:
            data = get_place(place_id=self.id, lang=lang)
            self.update_place(data=data, lang=lang)
            self.save()
        return self.formatted_address[lang]

    def get_parents_name(self, sub_type, lang=None):
        lang = self._lang(lang)
        name = self.parents_name.get(sub_type, {}).get(lang)
        if not name:
            data = get_place(place_id=self.id, lang=lang)
            self.update_place(data=data, lang=lang)
            self.save()
            name = self.parents_name.get(sub_type, {}).get(lang)
        return name

    def get_formatted_parents_name(self, sub_types, lang, sep=None):
        sep = sep or ', '
        names = [self.get_parents_name(name, lang) for name in sub_types]
        return sep.join([n for n in names if n])

    @classmethod
    def create(cls, data, lang):
        place = cls(id=data['place_id'], parents={}, parents_name={}, name={}, formatted_address={})
        place.update_place(data=data, lang=lang)
        place.save()
        return place

    def update_place(self, data, lang):
        self.types = data['types']
        self.name[lang] = data['name']
        self.formatted_address[lang] = data['formatted_address']
        self.point = Point(x=float(data['geometry']['location']['lng']), y=float(data['geometry']['location']['lat']))
        self._update_parents_name(data, lang)

    def _update_parents_name(self, data, lang):
        self.parents_name = self.parents_name or {}
        for ac in data['address_components']:
            for t in [t for t in ac.get('types') if t in self.ALLOWED_PARENTS_NAME]:
                if t not in self.parents_name:
                    self.parents_name[t] = {}
                self.parents_name[t][lang] = ac.get('long_name')
        return self.parents_name

    @classmethod
    def create_or_update(cls, data, lang):
        try:
            with atomic():
                place = cls.objects.get(id=data['place_id'])
                if lang not in place.name:
                    place.update_place(data, lang)
                    place.save()
        except cls.DoesNotExist:
            place = cls.create(data, lang)
        return place

    @classmethod
    def get_place(cls, place_id, lang=None):
        try:
            place = cls.objects.get(id=place_id)
            if lang and lang not in place.name or (place.date_update + timedelta(days=30) < timezone.now()):
                raise cls.DoesNotExist
        except cls.DoesNotExist:
            lang = cls._lang(lang)
            data = get_place(place_id=place_id, lang=lang)
            place = cls.create_or_update(data=data, lang=lang)
        return place

    def get_parent_place(self, result_type, lang=None):
        lang = self._lang(lang)
        if result_type in self.parents:
            place = self.get_place(place_id=self.parents[result_type], lang=lang)
        else:
            place = None
            data_list = get_reverse_geocode(latlng='{lat},{lng}'.format(lat=self.point.y, lng=self.point.x),
                                            result_type=result_type,
                                            lang=lang)
            if not data_list:
                return None

            for data in data_list:
                for ac in data['address_components']:
                    if result_type in ac['types']:
                        data['name'] = ac['long_name']
                        break

                if data.get('name'):
                    place = self.create_or_update(data=data, lang=lang)
                    self.parents[result_type] = place.id
                    self.save()
                    break
        return place


class Glocation(MP_Node):
    GLOCATION_ALLOWED_TYPE = ['locality', 'administrative_area_level_1', 'country']

    place_id = models.CharField(max_length=255, unique=True)
    type = models.CharField(_('type'), max_length=32, choices=[(n, n.upper()) for n in GLOCATION_ALLOWED_TYPE])
    name = JSONField(blank=True, default={})  # different language
    point = PointField(blank=True, null=True)

    class Meta:
        app_label = 'glocation'
        db_table = 'glocation'
        verbose_name = _('Google location')
        verbose_name_plural = _('Google locations')

    def __str__(self):
        return '%s<%s>' % (self.__class__.__name__, self.path)

    def __init__(self, *args, **kwargs):
        super(Glocation, self).__init__(*args, **kwargs)
        self.place_related = None

    @property
    def place(self):
        return Place.get_place(place_id=self.place_id)

    def get_name(self, lang=None):
        if lang not in self.name:
            self.name[lang] = self.place.get_name(lang)
            self.save()
        return self.name[lang]

    @classmethod
    def get_location(cls, place_id):
        return cls._get_location(place_id, parent_types=cls.GLOCATION_ALLOWED_TYPE)

    @classmethod
    def _get_location(cls, place_id, parent_types):

        if not parent_types:
            return None

        current_type = parent_types[0]
        parent_types = parent_types[1:]  # Search next parent

        try:
            node = cls.objects.get(place_id=place_id)

        except cls.DoesNotExist:
            place = Place.get_place(place_id=place_id)
            if not place:
                return None

            if current_type in place.types:
                place_related = None

            else:
                place_related = place
                place = place.get_parent_place(current_type)
                if not place:
                    return None
                if parent_types:
                    parent_types = parent_types[1:]  # Search next parent

            try:
                node = cls.objects.get(place_id=place.id)

            except cls.DoesNotExist:
                init_data = {
                    'place_id': place.id,
                    'type': current_type,
                    'name': place.name,
                    'point': place.point
                }

                if parent_types:
                    parent = cls._get_location(place_id=place.id, parent_types=parent_types)
                    if parent:
                        node = parent.add_child(**init_data)
                    else:
                        logger.error('Parents %s not found for place_id: %s' % (parent_types, place.id))
                        if not settings.GLOCATION_APPEND_ORPHAN:
                            raise ValueError('Parents %s not found for place_id: %s' % (parent_types, place.id))
                        else:
                            node = cls.add_root(**init_data)
                else:
                    node = cls.add_root(**init_data)

            node.place_related = place_related
        return node
