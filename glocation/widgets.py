# coding: utf-8
import six
from django import forms
from django.conf import settings
from django.contrib.gis.forms import Widget
from django.contrib.gis.geos import Point
from django.template import loader
from django.utils.safestring import mark_safe

from glocation.models import Place

__author__ = 'vadim'


class GooglePlaceFieldWidget(Widget):
    template_name = 'glocation/googleplacefieldwidget.html'

    def __init__(self, attrs=None, country=None, lang=None):
        super(GooglePlaceFieldWidget, self).__init__(attrs)
        self.country = country
        self.lang = lang or 'en'

    @property
    def media(self):
        css = {}
        js = [
            mark_safe("https://maps.googleapis.com/maps/api/js?libraries=places&key={}".format(settings.GOOGLE_PUBLIC_API_KEY)),
        ]

        return forms.Media(js=js, css=css)

    def render(self, name, value, attrs=None):
        if not attrs:
            attrs = dict()

        try:
            if value:
                loc = Place.get_place(place_id=value, lang=self.lang)
                value_name = loc.get_formatted_address(lang=self.lang) if loc else ''
            else:
                value_name = ''
        except Place.DoesNotExist:
            value_name = ''

        context = self.build_attrs(
            attrs,
            name=name,
            value=value,
            value_name=value_name,
            country=self.country,
            lang=self.lang
        )
        return loader.render_to_string(self.template_name, context)


class GoogleMapFieldWidget(Widget):
    template_name = 'glocation/googlemapfieldwidget.html'

    def __init__(self, attrs=None, input_init=None, skip_load_gmaps_js=None, map_style=None):
        super(GoogleMapFieldWidget, self).__init__(attrs)
        self.skip_load_gmaps_js = skip_load_gmaps_js
        self.input_init = input_init
        self.map_style = map_style or 'height:380px !important;'

    @property
    def media(self):
        css = {}
        js = [mark_safe('glocation/glocation-map.js')]
        if not self.skip_load_gmaps_js:
            js.append(
                mark_safe("https://maps.googleapis.com/maps/api/js?libraries=places&key={}".format(
                    settings.GOOGLE_PUBLIC_API_KEY))
            )

        return forms.Media(js=js, css=css)

    def render(self, name, value, attrs=None):
        if not attrs:
            attrs = dict()

        context = self.build_attrs(
            attrs,
            name=name,
            map_name='map_{}'.format(name),
            value='POINT({x} {y})'.format(x=value.x, y=value.y) if isinstance(value, Point) else value,
            input_init=self.input_init,
            map_style=self.map_style
        )
        return loader.render_to_string(self.template_name, context)
