/**
 * Created by vadim on 10.10.16.
 */

(function () {
    // Google Maps
    var map, marker,
        shop_gmap = {};
    var input_geopoint;

    function val_to_latlng(val) {
        var result = val.match( /POINT\s*\(\s*(\d+.\d+)\s+(\d+.\d+)\s*\)/i );
        if (result) {
            return new google.maps.LatLng(result[2], result[1]);
        }
        return null;
    }

    function set_marker(pos){
        marker.setPosition(pos);
        if (map.getZoom() <= 12) {
            map.setZoom(17);
        }
        map.setCenter(pos);
        input_geopoint.value = 'POINT(' + pos.lng() + ' ' + pos.lat() + ')';
        console.log(input_geopoint.value);
    }

    function init_by_option(opt) {
        // Init map
        map = new google.maps.Map(document.getElementById(opt.canvas), opt.mapOptions);

        // Init marker
        opt.markOptions.map = map;
        marker = new google.maps.Marker(opt.markOptions);

        if (opt.markOptions.draggable) {
            google.maps.event.addListener(marker, 'dragend', function(ev){
                set_marker(ev.latLng);
            });
        }
    }

    // Google Maps
    shop_gmap.init = function(opt) {
        input_geopoint = document.getElementById(opt.geopoint);

        var position = val_to_latlng(input_geopoint.value);
        if (position) {
            opt.mapOptions.zoom = 17;
            opt.mapOptions.center = position;
            opt.markOptions.position = position;
        }

        if (!position && opt.address_default) {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({'address': opt.address_default}, function(results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    opt.mapOptions.center = results[0].geometry.location;

                } else {
                    console.error('Geocode was not successful for the following reason: ' + status);
                }
                init_by_option(opt);
            });
        } else {
            init_by_option(opt);
        }
    };

    shop_gmap.move_marker = function (location) {
        set_marker(location);
    };

    // Make sure to export code on self when in a browser
    if (typeof self !== 'undefined') {
        self.shop_gmap = shop_gmap;
    }

    // Expose code as a CJS module
    if (typeof exports === 'object') {
        module.exports = shop_gmap;
    }

    return shop_gmap;
}());
