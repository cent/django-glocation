#!/usr/bin/env python
try:
    from setuptools import setup, find_packages
except ImportError:
    from distribute_setup import use_setuptools
    use_setuptools()
    from setuptools import setup, find_packages

import glocation


setup(
    name='django-glocation',
    version=glocation.__version__,
    description='Location API Client based on Google Place API',
    long_description='\n' + open('README').read(),
    author='Vadim Statishin',
    author_email='statishin@gmail.com',
    keywords='location api',
    license='BSD License',
    url='http://bitbucket.org/cent/django-glocation/',
    #tests_require = ['nose', 'webtest'],
    #test_suite='nose.collector',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Database',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    platforms='any',
    zip_safe=True,
    packages=find_packages(),
    include_package_data=True,

#    requires=['django', 'requests'],
    install_requires=['django', 'django-treebeard', 'googlemaps'],
)
